package bytes;

import java.text.Normalizer;

public class StringUtil {
    
    public static final String ES_PALINDROMO = "es Palindromo ";
    public static final String NO_ES_PALINDROMO = "no es Palindromo";
    
    public static String limpiaPalabra(String palabra){
        
        try{
            
            palabra = palabra.trim();
            palabra = palabra.toLowerCase();
            palabra = palabra.replace(" ", "");
            palabra = palabra.replace(",", "");
            palabra = palabra.replace(".", "");
            palabra = Normalizer.normalize(palabra, Normalizer.Form.NFD);
            palabra = Normalizer.normalize(palabra, Normalizer.Form.NFC);
            
        }catch(Exception e){
            e.printStackTrace();
        }

        return palabra;
    }
    
    
    public static String siEsPalindromo(String palabra){
      
        String resultado = "";
        
        try{
            
            resultado = palabra + "  " + ES_PALINDROMO;
            
        }catch(Exception e){
            e.printStackTrace();
        }

        return resultado;
    }
    
    public static String noEsPalindromo(String palabra){
      
        String resultado = "";
        
        try{
            
            resultado = palabra + " "+ NO_ES_PALINDROMO;
            
        }catch(Exception e){
            e.printStackTrace();
        }

        return resultado;
    }
    
}
