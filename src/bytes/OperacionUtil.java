package bytes;

public class OperacionUtil {
    
     public static boolean esPalindromo (String palabra) {
         
        int inicio = 0;
        boolean esPalindromo = true;
         
        try{
            
            palabra = StringUtil.limpiaPalabra(palabra);
            int fin  = palabra.length()-1;
         
            if(palabra.isEmpty()){
                return false;
            } 

            while(inicio < fin){
                if(palabra.charAt(inicio)!=palabra.charAt(fin)){
                    esPalindromo = false;
                }
            inicio++;
            fin--;
            }
                        
        }catch(Exception e){
            e.printStackTrace();
        }

        return esPalindromo; 
    }
    
}
