package bytes;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {

        try{
            
            Scanner scanner = new Scanner(System.in);
            
            System.out.print("Ingresa una Palabra ");
            String palabra = scanner.nextLine();
            
            if(OperacionUtil.esPalindromo(palabra)){
                System.out.println(StringUtil.siEsPalindromo(palabra));
            } else {
                System.out.println(StringUtil.noEsPalindromo(palabra));
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        
    }
    
}
